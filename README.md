# Modern Template - Sassify  
Created and Developed by Jack Ryan Oracoy  
  
jackryanoracoy@gmail.com  
lngrzproject@gmail.com   
  
  
PORTAL  
------------------------------------------------------------  
Github     :   https://github.com/jackryanoracoy  
Facebook   :   https://facebook.com/JackRyanOracoy  
Twitter    :   https://twitter.com/JackRyanOracoy  
Behance    :   https://www.behance.net/jackryanor7dac  
LinkedIn   :   https://www.linkedin.com/in/jackryanoracoy  
  
  
ABOUT  
------------------------------------------------------------  
A Progressive Web App (PWA) Template and is built using the most mature, stable, and powerful professional grade CSS extension language in the world (SASS).  
  
  
CONTENT  
------------------------------------------------------------  
Codebase; HTML5, CSS3, JQuery, SASS.  
Pagespeed optimized codebase.  
Service workers (Offline Cache).  
App icons and favicons.   
  
  
MORE INFORMATION  
------------------------------------------------------------  
SASS Files: common/scss/  
  
_base.scss - Base style and browser fixes.  
_fonts.scss - Font variations, located at 'common/scss/fonts/* and common/fonts/*'.  
_component.scss - List of components, located at 'common/scss/component/*'.  
_layout.scss - List of layouts, located at 'common/scss/loyout/*'.  
_utility.scss - List of utilities, located at 'common/scss/utilitie/*'.  
_mixin.scss -  Some mixins.  
_print.scss - Style for print.   
_variable.scss - Variable used for styles.  